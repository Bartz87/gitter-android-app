package im.gitter.gitter.network;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import im.gitter.gitter.content.ModelFactory;
import im.gitter.gitter.models.Group;
import im.gitter.gitter.models.Room;
import im.gitter.gitter.models.User;

public abstract class RoomsRequest extends ApiRequest<Set<String>> {

    private final ModelFactory modelFactory = new ModelFactory();
    private final ContentResolver contentResolver;

    public RoomsRequest(Context context, String path, com.android.volley.Response.Listener<Set<String>> listener, com.android.volley.Response.ErrorListener errorListener) {
        super(context, path, listener, errorListener);
        this.contentResolver = context.getContentResolver();
    }

    @Override
    protected Set<String> parseJsonInBackground(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);

        // map by id to remove all duplicates
        Map<String, ContentValues> roomsById = new HashMap<>();
        Map<String, ContentValues> usersById = new HashMap<>();
        Map<String, ContentValues> groupsById = new HashMap<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject json = jsonArray.getJSONObject(i);

            Room room = modelFactory.createRoom(json);

            roomsById.put(room.getId(), room.toContentValues());

            if (!json.isNull("user")) {
                User user = modelFactory.createUser(json.getJSONObject("user"));
                usersById.put(user.getId(), user.toContentValues());
            }

            if (!json.isNull("group")) {
                Group group = modelFactory.createGroup(json.getJSONObject("group"));
                groupsById.put(group.getId(), group.toContentValues());
            }
        }

        writeToDatabaseInBackground(contentResolver, toArray(roomsById), toArray(usersById), toArray(groupsById));

        return roomsById.keySet();
    }

    protected abstract void writeToDatabaseInBackground(ContentResolver contentResolver, ContentValues[] rooms, ContentValues[] users, ContentValues[] groups);

    private ContentValues[] toArray(Map<String, ContentValues> contentValuesById) {
        return contentValuesById.values().toArray(new ContentValues[contentValuesById.size()]);
    }
}
